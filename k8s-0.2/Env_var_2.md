# Вариант 2.
## `ConfigMap`
Создаем файл `configmap`
```yaml
vim my-configmap.yaml
```
Содержимое файла.
```yaml
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: my-configmap
data:
  SECOND: value2
```
В `ConfigMap` имеется раздел `data`, собственно там мы и задаём наши настройки, а потом этот словарь, этот `ConfigMap`, целиком указать в манифесте деплоймента, чтобы из него создать соответствующие переменные окружения в нашем контейнере.
Таким образом, за счёт `ConfigMap` мы можем уменьшить дублирование кода и упростить настройку однотипных приложений. Один и тот же `ConfigMap` можно монтировать в разные поды. 

Изменяем наш `deployment`.
```yaml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-deployment
spec:
  replicas: 2 
  selector:
    matchLabels:
      app: my-app
  revisionHistoryLimit: 10
  template:
    metadata:
      labels:
        app: my-app
    spec:
      containers:
      - image: nginxdemos/hello:0.2
        name: firstapp
        env:                      # Жестко прописанная переменная (Вариант 1)
        - name: ONE               # Имя переменной
          value: My_test_value    # Значение переменной
        envFrom:                  # раздел для загрузки переменных окружения извне
        - configMapRef:           # указываем, что будем брать их по ссылке на конфигмап
            name: my-configmap    # указываем наш объект ConfigMap, из которого будут загружаться данные
        ports:
        - containerPort: 80
        readinessProbe:
          failureThreshold: 3
          httpGet:
            path: /
            port: 80
          periodSeconds: 10
          successThreshold: 1
          timeoutSeconds: 1
        livenessProbe:
          failureThreshold: 3
          httpGet:
            path: /
            port: 80
          periodSeconds: 10
          successThreshold: 1
          timeoutSeconds: 1
          initialDelaySeconds: 10
        startupProbe:
          httpGet:
            path: /
            port: 80
          failureThreshold: 30
          periodSeconds: 10
```
Применяем наши файлы.
```bash
kubectl apply -f .  # Точка в конце говорит применить все файлы из каталога в котором мы находимся
```
В результате должны получить `configmap` и `deployment`. Проверяем
```bash
kubectl get cm  # cm сокращение от configmap
```
Вывод:
```bash
NAME               DATA   AGE
kube-root-ca.crt   1      35d
my-configmap       1      24m
```
+ Тут `kube-root-ca.crt` - `configmap kubernetes` (сертификат)
+ `my-configmap` - созданный нами 
  - DATA говорит нам о наличии 1 ключа.
   
Загляним в наш под (любой).
```bash
kubectl describe my-deployment-5c47bdf7d4-6dlpg
````
Вывод:
```yaml
--- # Интересует секция Containers
    Environment Variables from:                   # Переменная полученная из cjnfigmap
      my-configmap  ConfigMap  Optional: false    # Показывается откуда получена переменная. Значения не показываются
    Environment:              # Переменная заданная вручную
      ONE:  My_test_value     # Сама переменная и ее значение
---- # Конец
```
Что бы увидеть переменную и ее значение, нам необходимо зайти в под и там прочитать `ENV`
```bash
kubectl exec -ti my-deployment-5c47bdf7d4-6dlpg -- env
```
Вывод:
```yaml
[...]
  SECOND=value2       # Переменная из configmap
  ONE=My_test_value   # Переменная заданная в ручную
[...]
```
------
***Примечание.***

- Переменные заданные в ручную и прописанные в `deployment` имеют приоритет над переменными указаных в `configmap`. 
- То есть, если имена переменных совпадают, то будет взято значение указанное в `deployment` а не из `configmap`.


----
+ ### К оглавлению! [___Оглавление___](./README.md)