# `Configmap` как файл конфигурации приложения
Использовать `configmap` можно и для передачи конфигурации в приложение. 

На примере `nginx`.
Посмотрим конфиг нащего `nginx`.
+ 1. Где он лежит и как называется
```bash
kubectl exec -ti my-deployment-5c47bdf7d4-6dlpg -- ls /etc/nginx/conf.d/
[...]
hello.conf    # Имя конфиг файл нашего nginx
```
+ 2. Содержимое файла.
```bash
kubectl exec -ti my-deployment-5c47bdf7d4-6dlpg -- cat /etc/nginx/conf.d/hello.conf
[...]
server {
    listen 80;

    root /usr/share/nginx/html;
    try_files /index.html =404;

    expires -1;

    sub_filter_once off;
    sub_filter 'server_hostname' '$hostname';
    sub_filter 'server_address' '$server_addr:$server_port';
    sub_filter 'server_url' '$request_uri';
    sub_filter 'server_date' '$time_local';
    sub_filter 'request_id' '$request_id';
}
```
+ 3. Сделаем `port-forward` и посмотрим `curl` на имеющиеся поды.
```bash
kubectl port-forward my-deployment-5c47bdf7d4-6dlpg 8181:80 & # Указываем локальный порт 8181 и мапим его на 80 порт контейнера
curl localhost:8181   # Делаем запрос curl`ом

- # Содержимое нашего index.html (Приведен не полностью а небольшая выдержка)
<!DOCTYPE html>
<html>
<head>
<title>Hello World</title>
[...]
<div class="info">
<p><span>Server&nbsp;address:</span> <span>127.0.0.1:80</span></p>
<p><span>Server&nbsp;name:</span> <span>my-deployment-5c47bdf7d4-6dlpg</span></p>
<p class="smaller"><span>Date:</span> <span>04/Jan/2022:16:34:45 +0000</span></p>
<p class="smaller"><span>URI:</span> <span>/</span></p>
</div>
<div class="check"><input type="checkbox" id="check" onchange="changeCookie()"> Auto Refresh</div>
    <div id="footer">
        <div id="center" align="center">
            Request ID: db2e7d878ad6f9dc4f8b64f4f60274f3<br/>
            &copy; NGINX, Inc. 2018
        </div>
    </div>
</body>
</html>
```
Давайте создадим файл `conf-configmap.yaml` с таким содержанием:
```bash
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: conf-configmap
data:
  hello.conf: |     # Обратите внимание на название файла
    server {
        listen       80 default_server;
        server_name  _;

        default_type text/plain;

        location / {
            return 200 '$hostname\n','$server_addr:$server_port\n';
        }
    }
```
В данном примере мы создаем конфиг для `nginx`. 

Данный конфиг будет монтироваться в под и конфигурировать `nginx`. В секции `data` через конвеер/pipe (символ `"|"`) указываем многострочный конфиг.

Теперь необходимо изменить `deployment`.
```yaml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-deployment
spec:
  replicas: 2 
  selector:
    matchLabels:
      app: my-app
  revisionHistoryLimit: 10
  template:
    metadata:
      labels:
        app: my-app
    spec:
      containers:
      - image: nginxdemos/hello:0.2
        name: firstapp
        env:
        - name: ONE
          value: My_test_value
        envFrom:
        - configMapRef:
            name: my-configmap
        ports:
        - containerPort: 80
        readinessProbe:
          failureThreshold: 3
          httpGet:
            path: /
            port: 80
          periodSeconds: 10
          successThreshold: 1
          timeoutSeconds: 1
        livenessProbe:
          failureThreshold: 3
          httpGet:
            path: /
            port: 80
          periodSeconds: 10
          successThreshold: 1
          timeoutSeconds: 1
          initialDelaySeconds: 10
        startupProbe:
          httpGet:
            path: /
            port: 80
          failureThreshold: 30
          periodSeconds: 10
        volumeMounts:                       # Здесь мы описываем точки монтирования томов внутри контейнера
        - name: config                      # Указываем имя тома для монтирования
          mountPath: /etc/nginx/conf.d/hello.conf     # Здесь мы указываем точку монтирования 
          subPath: hello.conf
      volumes:                          # Здесь (на уровень выше!) мы описываем тома
      - name: config                    # Задаём имя тома
        configMap:                      # Указываем, из какого конфигмапа создать том
          name: conf-configmap
```
Тут мы в секции `containers` монтируем наш `volume`.

А на уровень выше, в секции `spec` описываем наш `volume`.

Таким образом мы монтируем том как файл конфигурации нашего `nginx`.

Теперь применим наши файлы.
```bash
kubectl apply -f conf-configmap.yaml
kubectl apply -f my-deployment.yaml
```
Проверим что получилось.
```bash
kubectl get cm 
```
Вывод:
```yaml
NAME               DATA   AGE
conf-configmap     1      29m
kube-root-ca.crt   1      36d
my-configmap       1      29m
```
```bash
kubectl get po -w
```
Вывод:
```yaml
NAME                             READY   STATUS              RESTARTS   AGE
my-deployment-6d669878bb-fqnsb   0/1     ContainerCreating   0          5s
my-deployment-76697ccd9b-fvw8r   1/1     Running             0          2m58s
my-deployment-76697ccd9b-phnjm   1/1     Running             0          3m30s
my-deployment-6d669878bb-fqnsb   0/1     Running             0          9s
my-deployment-6d669878bb-fqnsb   0/1     Running             0          10s
my-deployment-6d669878bb-fqnsb   1/1     Running             0          20s
my-deployment-76697ccd9b-phnjm   1/1     Terminating         0          3m46s
my-deployment-6d669878bb-zzb5t   0/1     Pending             0          0s
my-deployment-6d669878bb-zzb5t   0/1     Pending             0          0s
my-deployment-6d669878bb-zzb5t   0/1     ContainerCreating   0          1s
my-deployment-76697ccd9b-phnjm   1/1     Terminating         0          3m49s
my-deployment-6d669878bb-zzb5t   0/1     ContainerCreating   0          8s
my-deployment-76697ccd9b-phnjm   0/1     Terminating         0          3m54s
my-deployment-6d669878bb-zzb5t   0/1     Running             0          11s
my-deployment-76697ccd9b-phnjm   0/1     Terminating         0          4m2s
my-deployment-76697ccd9b-phnjm   0/1     Terminating         0          4m2s
my-deployment-6d669878bb-zzb5t   0/1     Running             0          20s
my-deployment-6d669878bb-zzb5t   1/1     Running             0          30s
my-deployment-76697ccd9b-fvw8r   1/1     Terminating         0          3m45s
my-deployment-76697ccd9b-fvw8r   1/1     Terminating         0          3m46s
my-deployment-76697ccd9b-fvw8r   0/1     Terminating         0          3m49s
```
Как видим, происходит `rolling update` и наши поды обновляются.

После обновления посмотрим `curl` в наши поды, предварительно запустив `port-forward`.
```bash
kubectl port-forward my-deployment-6d669878bb-fqnsb 8181:80 & # Указываем локальный порт 8181 и мапим его на 80 порт контейнера
curl localhost:8181   # Делаем запрос curl`ом
[....]
my-deployment-6d669878bb-fqnsb
127.0.0.1:80
[....]
```
Как видим, вывод изменился. Таким образом мы смонтировали `configmap` в виде конфиг файла для нашего `nginx`. 

------
***Примечание.***

----
+ ### К оглавлению! [___Оглавление___](./README.md)