# Обновление ConfigMap на лету

По условию задания
```bash
Написать configmap так, чтобы её обновление делалось на лету в контейнере.
Внедрить это в Deployment и показать пример.
Какую особенность нужно соблюдать, чтобы это сработало?
```
Как всегда очищаем кластер и делаем все с нуля для лучшего закрепления материала.  
```bash
kubectl delete -f .   # Данная команда удаляет все примененные файлы из нашей директории
```
Для экспериментов возмем `nginx`. Файл `deployment` самый простой.
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: star
  namespace: default
  labels:
    app: star
spec:
  replicas: 1
  selector:
    matchLabels:
      app: star
  template:
    metadata:
      labels:
        app: star
    spec:
      containers:
      - name: nginx
        image: nginx:stable-alpine
```
Применяем его 
```bash
kubectl apply -f star.yaml    # Так назвал файл deployment
```
Вывод:
```yaml
kubectl get pod
[...]
NAME                    READY   STATUS    RESTARTS   AGE
star-846df6c84f-tdv7l   1/1     Running   0          8s
```
Видим что наш `deployment` применился и у нас имеется под с нашим приложением `nginx`.  
Загляним в него используя `port-forward` и `curl`, а так же посмотрим где корневая директория сервира `nginx`
```bash
kubectl port-forward star-846df6c84f-tdv7l 8181:80    # Запускаем port-forward и мапим порт 8181 локальной машины на порт 80 контейнера с nginx
[...]
curl localhost:8181     # Смотрим в контейнер
```
Вывод
```yaml
# Стандартная страничка nginx
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```
Посмотрим где распологается корень нашего сервера `nginx` в поде. Он может быть переопределен. Для этого посмотрим командой `ls` в под.
```bash
kubectl exec -ti star-846df6c84f-tdv7l -- ls -la /usr/share/nginx/html
```
Вывод:
```yaml
drwxr-xr-x    2 root     root          4096 Nov 16 18:22 .
drwxr-xr-x    3 root     root          4096 Nov 16 18:22 ..
-rw-r--r--    1 root     root           494 Nov 16 15:04 50x.html
-rw-r--r--    1 root     root           612 Nov 16 15:04 index.html
```
Видим по стандартному пути файл `index.html` и файл с ошибкой `50x.html`.  
И так. На данный момент у нас все работает, все запущенно стандартно. Давайте попробуем переназначить файл `index.html`.  
Для этого создадим простой файл `index.html` у нас в директории. Из него создадим `configmap` и применим в кластер..
Простой файл `index.html`
```html
<html>
<head>
  <title>Тестовая страница</title>
  <meta charset="UTF-8">
  <style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
  </style>
</head>
<body>
  <div align=center>
    <div align=center>
      <h1>Тестовая страница</h1>
    </div>
    <div align=center>
      <p>Простая тестовая страница</p>
      <p>Выводит статическую информацию</p>
      <p>добавленную в этот файл</p>
      <p>в рамках задания по kubernetes № k8s 0.2</p>
      <p>Задание со звездочкой</p>
    </div>
  </div>
</body>
</html>
```
Теперь нам требуется из него создать `configmap`. `ConfigMap` создать мы можем из фала или из директории. В данном случае нам надо создать из файла.  
```bash
kubectl create configmap index-html \       # Создаем configmap
  --from-file=./index.html --dry-run=client \         # Указываем из какого файла создать
  -o yaml | sed '/creationTimestamp/d' > index-html-01.yaml   # через sed убираем Timestamp и результат сохраняем в файл
```  
* Примечание. [^1]: Использование `sed` позволяет убрать из файла директиву `creationTimestamp`, устраняем проблему с обновлениями файла в дальнейшем.
```yaml
apiVersion: v1
data:
  index.html: |-
    <html>
    <head>
      <title>Тестовая страница</title>
      <meta charset="UTF-8">
      <style>
        body {
          width: 35em;
          margin: 0 auto;
          font-family: Tahoma, Verdana, Arial, sans-serif;
        }
      </style>
    </head>
    <body>
      <div align=center>
        <div align=center>
          <h1>Тестовая страница</h1>
        </div>
        <div align=center>
          <p>Простая тестовая страница</p>
          <p>Выводит статическую информацию</p>
          <p>добавленную в этот файл</p>
          <p>в рамках задания по kubernetes № k8s 0.2</p>
          <p>Задание со звездочкой</p>
        </div>
      </div>
    </body>
    </html>
kind: ConfigMap
metadata:
  name: index-html
```
И так, `configmap` создан. Теперь отредактируем наш `deployment`. Добавим в него такие строки
```yaml
[...] # Данная секция добавляется в template.spec.containers
  volumeMounts:
  - name: index-html
    mountPath: /usr/share/nginx/html/index.html
    subPath: index.html
[...] # Данная секция помещается на уровень выше, то есть в template.spec
volumes:
- name: index-html
  configMap:
  name: index-html
```
Пробуем применить наш `configmap` и `deployment`
```bash
kubectl apply -f index-html-01.yaml   # Наш configmap
kubectl apply -f star.yaml            # Наш deployment
```  
Проверяем
```bash
kubectl apply -f index-html-01.yaml
[...]
configmap/index-html created
[...]
kubectl apply -f star.yaml
[...]
deployment.apps/star configured
```  
Смотрим что происходит
```bash
kubectl get po -w
[...]
NAME                    READY   STATUS              RESTARTS   AGE
star-57985cdf8d-4wb65   0/1     ContainerCreating   0          4s
star-846df6c84f-tdv7l   1/1     Running             0          52m
star-57985cdf8d-4wb65   1/1     Running             0          7s
star-846df6c84f-tdv7l   1/1     Terminating         0          52m
star-846df6c84f-tdv7l   1/1     Terminating         0          52m
star-846df6c84f-tdv7l   0/1     Terminating         0          52m
star-846df6c84f-tdv7l   0/1     Terminating         0          52m
star-846df6c84f-tdv7l   0/1     Terminating         0          52m
```
Как видим у нас произошло пересоздание подов. Загляним в под используя `curl` через `port-forward`
```bash
kubectl port-forward star-57985cdf8d-4wb65 8181:80

[Вывод ->]
Forwarding from 127.0.0.1:8181 -> 80
Forwarding from [::1]:8181 -> 80

[...]
curl localhost:8181

[Вывод ->]
<html>
<head>
  <title>Тестовая страница</title>
  <meta charset="UTF-8">
  <style>
    body {
      width: 35em;
      margin: 0 auto;
      font-family: Tahoma, Verdana, Arial, sans-serif;
    }
  </style>
</head>
<body>
  <div align=center>
    <div align=center>
      <h1>Тестовая страница</h1>
    </div>
    <div align=center>
      <p>Простая тестовая страница</p>
      <p>Выводит статическую информацию</p>
      <p>добавленную в этот файл</p>
      <p>в рамках задания по kubernetes № k8s 0.2</p>
      <p>Задание со звездочкой</p>
    </div>
  </div>
</body>
</html>  
```
В данном случае мы видим что нам отдается наш `index.html` который мы создали и примонтировали в под вместо дефолного `index.html` используя путь внутри контейнера `/usr/share/nginx/html/index.html (#mountPath)`. А директивой `subPath` мы явно указали в какой файл поместить наш конфиг.  
Данный пример показывает как использовать `configmap` для конфигурационных файлов. Надо иметь ввиду. что это работает только при старте пода. То есть. если вы изменили `configmap` то его изменения никак не отразятся на приложении пока не будт рестартован `deployment` и соответственно не уничтожатся старые поды и не создадуться новые.  
Для того что бы наш `configmap` применялся без пересоздания подов. нам необходимо монтировать его не как файл (параметр `subPath`) а как директорию (аналог `volumes` в `docker`).  
Создадим копию нашего `deployment`. Пусть будет `star-02.yaml`.  
Отредактируем секцию с `volumeMounts`. Приведем в такой вид.
```yaml
[...]
  volumeMounts:
  - name: index-html
    mountPath: /usr/share/nginx/html/     # Тут убрали имя файла index.html и удалили директиву subPath
[...]
```
Применим наш `deployment`, убедимся что под пересоздался, посмотрим `curl` и убедимся что ничего **НЕ** изменилось в выводе.  
Далее отредактируем наш `configmap`. К примеру добавим фон к тегу `body` и добавим еще один блок со строкой. Пример:
```html
<html>
<head>
  <title>Тестовая страница</title>
  <meta charset="UTF-8">
  <style>
    body {
      width: 35em;
      margin: 0 auto;
      font-family: Tahoma, Verdana, Arial, sans-serif;
      background: #fa8e47;      # Добавили фон
    }
  </style>
</head>
<body>
  <div align=center>
    <div align=center>
      <h1>Тестовая страница</h1>
    </div>
    <div align=center>
      <p>Простая тестовая страница</p>
      <p>Выводит статическую информацию</p>
      <p>добавленную в этот файл</p>
      <p>в рамках задания по kubernetes № k8s 0.2</p>
      <p>Задание со звездочкой</p>
      <p>Дополнительная строка, добавленная в ConfigMap</p>     # Добавили строку
    </div>
  </div>
</body>
</html> 
```
Теперь применим наш `configmap` 
```bash
kubectl apply -f index-html-01.yaml
```
Запустим `port-forward` и перейдем в браузере по адресу `http://localhost:8181`.  
Нужно подождать какое то время (вроде в документации написано что 2 минуты) что бы `kubernetes` обновил информацио о `configmap`  
Через пару минут можем обновить страницу в браузере и увидим что фон страницы поменялся и добавилась строка внизу.  
## Ура. Мы сделали это

----
+ ### К оглавлению! [___Оглавление___](./README.md)