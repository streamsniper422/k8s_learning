# Работа с аннотациями
По условию задания требуется:
```bash
Проставить аннотации у подов деплоймента и применить их на лету без простоя приложения.
Какую особенность вы в этом увидели?(в видео упоминается).
```
----
Всю практику будем проводить с нашими обновленными файлами. То есть, с последним измененным `deployment`.

Для начала сделаем `describe` пода и посмотрим его аннотации.
```bash
kubectl describe po my-deployment-6d669878bb-fqnsb | grep -C 5 -B 2 Annotations
```
Вывод:
```yaml
Labels:       app=my-app
              pod-template-hash=6d669878bb
Annotations:  cni.projectcalico.org/containerID: 0772ecab2ec0a1c061d52114a6cdaafb35970f4ebf7dacbf5be232b883638ccf
              cni.projectcalico.org/podIP: 10.233.117.140/32
              cni.projectcalico.org/podIPs: 10.233.117.140/32
              kubectl.kubernetes.io/restartedAt: 2022-01-04T20:22:54+03:00
Status:       Running
IP:           10.233.117.140
```
Видим что у нас в аннотациях присутствует описание `cni` и время последнего рестарта.

Попробуем на лету добавить аннотацию к поду через наш `deployment`. Поместим метку `environment: develop` указывающая что приложение работает в `developed` среде.

Редактируем `deployment` на лету:
```bash
kubectl edit deploy my-deployment
```
Вызывается редактор (`vim`) и мы можем отредактировать файл, добавив строку в секцию отвечающую за аннотацию подов.
```bash
# В секции template.metadata.annotations
  template:
    metadata:
      annotations:
        environment: develop      # Добовляем строку с меткой
        kubectl.kubernetes.io/restartedAt: "2022-01-04T20:22:54+03:00"
        creationTimestamp: null
        labels:
          app: my-app
      spec:
        containers:
```
Сохраняем и выходим из `vim`.

Поскольку мы делали изменения в `tempalete pod`, то `kubernetes` следуя правилам `rolling update` перезапустит наши поды, что мы можем увидить введя команду
```bash
kubectl get pod -w

NAME                             READY   STATUS              RESTARTS   AGE
my-deployment-6c978948b8-rtwv2   0/1     ContainerCreating   0          8s
my-deployment-6d669878bb-fqnsb   1/1     Running             0          85m
my-deployment-6d669878bb-zzb5t   1/1     Running             0          84m
my-deployment-6c978948b8-rtwv2   0/1     Running             0          9s
my-deployment-6c978948b8-rtwv2   0/1     Running             0          10s
my-deployment-6c978948b8-rtwv2   1/1     Running             0          20s
my-deployment-6d669878bb-fqnsb   1/1     Terminating         0          85m
my-deployment-6c978948b8-h7pm7   0/1     Pending             0          1s
my-deployment-6c978948b8-h7pm7   0/1     Pending             0          1s
my-deployment-6c978948b8-h7pm7   0/1     ContainerCreating   0          2s
my-deployment-6d669878bb-fqnsb   1/1     Terminating         0          85m
my-deployment-6c978948b8-h7pm7   0/1     ContainerCreating   0          8s
my-deployment-6d669878bb-fqnsb   0/1     Terminating         0          85m
my-deployment-6d669878bb-fqnsb   0/1     Terminating         0          85m
my-deployment-6d669878bb-fqnsb   0/1     Terminating         0          85m
my-deployment-6c978948b8-h7pm7   0/1     Running             0          12s
my-deployment-6c978948b8-h7pm7   0/1     Running             0          21s
my-deployment-6c978948b8-h7pm7   1/1     Running             0          31s
my-deployment-6d669878bb-zzb5t   1/1     Terminating         0          85m
my-deployment-6d669878bb-zzb5t   1/1     Terminating         0          85m
my-deployment-6d669878bb-zzb5t   0/1     Terminating         0          85m
my-deployment-6d669878bb-zzb5t   0/1     Terminating         0          85m
my-deployment-6d669878bb-zzb5t   0/1     Terminating         0          85m
```
После обновления приложения, мы можем увидить добавленную аннотацию у наших подов. 
```bash
kubectl describe po my-deployment-6c978948b8-h7pm7 | grep -C 5 -B 2 Annotations
```
Вывод:
```yaml
Labels:       app=my-app
              pod-template-hash=6c978948b8
Annotations:  cni.projectcalico.org/containerID: be0d37cb0ac86b7a18229d8f8c53e4f349b9d4e6d510a3acf1db31434e1d9abc
              cni.projectcalico.org/podIP: 10.233.99.140/32
              cni.projectcalico.org/podIPs: 10.233.99.140/32
              environment: develop      # Как видим в аннотацию добавилась строка
              kubectl.kubernetes.io/restartedAt: 2022-01-04T20:22:54+03:00
Status:       Running
```
Таким образом мы добавили аннотацию к подам, а `kubernetes` обновил нам приложение без простоя (`rolling update`), конечно если выбрана схема `RollingUpdate` а не `ReCreate`. 






----
+ ### К оглавлению! [___Оглавление___](./README.md)