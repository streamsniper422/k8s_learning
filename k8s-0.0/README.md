# Создание и установка кластера kubernetes

Кластер развернут на своих мошностях. `(KVM Proxmox)` 3 виртуальные машины в виде
+ 1 - `Master`
+ 2 - `Worker`

Разворачивался через kubespray `tag - v2.17.1`

Итоги:
+ Получаем список `nodes`
```bash
kubectl get nodes
NAME    STATUS   ROLES                  AGE     VERSION
k8s-1   Ready    control-plane,master   2d18h   v1.21.6
k8s-2   Ready    <none>                 2d18h   v1.21.6
k8s-3   Ready    <none>                 2d18h   v1.21.6
```
+ Получаем список `namespace`
```bash
kubectl get ns
NAME              STATUS   AGE  
cert-manager      Active   2d18h
default           Active   2d18h
ingress-nginx     Active   2d18h
kube-node-lease   Active   2d18h
kube-public       Active   2d18h
kube-system       Active   2d18h
```
+ Получаем список `ingress pod in namespace ingress-nginx`
```bash
kubectl get pod -n ingress-nginx
NAME                             READY   STATUS    RESTARTS   AGE
ingress-nginx-controller-rt8dg   1/1     Running   9          4d2h
ingress-nginx-controller-vlcq7   1/1     Running   7          4d2h
```
+ Получаем список запущенных `pods in namespace kube-system`
```bash
kubectl get pods -n kube-system
NAME                                       READY   STATUS    RESTARTS   AGE
calico-kube-controllers-8575b76f66-d77rq   1/1     Running   4          2d18h
calico-node-4z8fk                          1/1     Running   2          2d18h
calico-node-kldn8                          1/1     Running   2          2d18h
calico-node-kmnvn                          1/1     Running   2          2d18h
coredns-8474476ff8-dxxcj                   1/1     Running   2          2d18h
coredns-8474476ff8-lszvz                   1/1     Running   2          2d18h
dns-autoscaler-7df78bfcfb-nk2fr            1/1     Running   2          2d18h
etcd-k8s-1                                 1/1     Running   2          2d18h
kube-apiserver-k8s-1                       1/1     Running   2          2d18h
kube-controller-manager-k8s-1              1/1     Running   3          2d18h
kube-proxy-q9w2m                           1/1     Running   2          2d18h
kube-proxy-s8zsw                           1/1     Running   2          2d18h
kube-proxy-v5q2z                           1/1     Running   2          2d18h
kube-scheduler-k8s-1                       1/1     Running   3          2d18h
nginx-proxy-k8s-2                          1/1     Running   2          2d18h
nginx-proxy-k8s-3                          1/1     Running   2          2d18h
nodelocaldns-2b294                         1/1     Running   2          2d18h
nodelocaldns-2sgxx                         1/1     Running   2          2d18h
nodelocaldns-njzgg                         1/1     Running   1          2d18h
```
+ Получаем список `deployment in namespace kube-system`
```bash
kubectl get deploy -n kube-system
NAME                      READY   UP-TO-DATE   AVAILABLE   AGE
calico-kube-controllers   1/1     1            1           2d18h
coredns                   2/2     2            2           2d18h
dns-autoscaler            1/1     1            1           2d18h
```
+ Получаем список `services`
```bash
kubectl get service
NAME         TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
kubernetes   ClusterIP   10.233.0.1   <none>        443/TCP   2d18h
```
+ Получить _расширенную_ информацию о нодах (ключ _`-o wide`_)
```bash
kubectl get node -o wide
NAME    STATUS   ROLES                  AGE     VERSION   INTERNAL-IP     EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION       CONTAINER-RUNTIME
k8s-1   Ready    control-plane,master   2d18h   v1.21.6   172.16.10.191   <none>        Ubuntu 18.04.6 LTS   4.15.0-163-generic   docker://20.10.8
k8s-2   Ready    <none>                 2d18h   v1.21.6   172.16.10.192   <none>        Ubuntu 18.04.6 LTS   4.15.0-163-generic   docker://20.10.8
k8s-3   Ready    <none>                 2d18h   v1.21.6   172.16.10.193   <none>        Ubuntu 18.04.6 LTS   4.15.0-163-generic   docker://20.10.8
```
+ Получить _подробную_ информацию по node _`k8s-1`_
```bash
kubectl describe node k8s-1
Name:               k8s-1
Roles:              control-plane,master
Labels:             beta.kubernetes.io/arch=amd64
                    beta.kubernetes.io/os=linux
                    kubernetes.io/arch=amd64
                    kubernetes.io/hostname=k8s-1
                    kubernetes.io/os=linux
                    node-role.kubernetes.io/control-plane=
                    node-role.kubernetes.io/master=
                    node.kubernetes.io/exclude-from-external-load-balancers=
Annotations:        kubeadm.alpha.kubernetes.io/cri-socket: /var/run/dockershim.sock
                    node.alpha.kubernetes.io/ttl: 0
                    projectcalico.org/IPv4Address: 172.16.10.191/24
                    projectcalico.org/IPv4IPIPTunnelAddr: 10.233.95.0
                    volumes.kubernetes.io/controller-managed-attach-detach: true
CreationTimestamp:  Mon, 29 Nov 2021 20:24:09 +0300
Taints:             node-role.kubernetes.io/master:NoSchedule
Unschedulable:      false
Lease:
  HolderIdentity:  k8s-1
  AcquireTime:     <unset>
  RenewTime:       Thu, 02 Dec 2021 15:08:45 +0300
Conditions:
  Type                 Status  LastHeartbeatTime                 LastTransitionTime                Reason                       Message
  ----                 ------  -----------------                 ------------------                ------                       -------
  NetworkUnavailable   False   Thu, 02 Dec 2021 14:32:24 +0300   Thu, 02 Dec 2021 14:32:24 +0300   CalicoIsUp                   Calico is running on this node
  MemoryPressure       False   Thu, 02 Dec 2021 15:08:42 +0300   Mon, 29 Nov 2021 20:23:59 +0300   KubeletHasSufficientMemory   kubelet has sufficient memory available
  DiskPressure         False   Thu, 02 Dec 2021 15:08:42 +0300   Mon, 29 Nov 2021 20:23:59 +0300   KubeletHasNoDiskPressure     kubelet has no disk pressure
  PIDPressure          False   Thu, 02 Dec 2021 15:08:42 +0300   Mon, 29 Nov 2021 20:23:59 +0300   KubeletHasSufficientPID      kubelet has sufficient PID available
  Ready                True    Thu, 02 Dec 2021 15:08:42 +0300   Mon, 29 Nov 2021 20:28:29 +0300   KubeletReady                 kubelet is posting ready status. AppArmor enabled
Addresses:
  InternalIP:  172.16.10.191
  Hostname:    k8s-1
Capacity:
  cpu:                4
  ephemeral-storage:  10188088Ki
  hugepages-2Mi:      0
  memory:             4038716Ki
  pods:               110
Allocatable:
  cpu:                3800m
  ephemeral-storage:  9389341886
  hugepages-2Mi:      0
  memory:             3412028Ki
  pods:               110
System Info:
  Machine ID:                 dcd86a91133d4c6c8a1f80a906c60fee
  System UUID:                DCD86A91-133D-4C6C-8A1F-80A906C60FEE
  Boot ID:                    164b835b-9eea-4aad-bdc0-80b9bcd34534
  Kernel Version:             4.15.0-163-generic
  OS Image:                   Ubuntu 18.04.6 LTS
  Operating System:           linux
  Architecture:               amd64
  Container Runtime Version:  docker://20.10.8
  Kubelet Version:            v1.21.6
  Kube-Proxy Version:         v1.21.6
PodCIDR:                      10.233.64.0/24
PodCIDRs:                     10.233.64.0/24
Non-terminated Pods:          (7 in total)
  Namespace                   Name                             CPU Requests  CPU Limits  Memory Requests  Memory Limits  Age
  ---------                   ----                             ------------  ----------  ---------------  -------------  ---
  kube-system                 calico-node-kldn8                150m (3%)     300m (7%)   64M (1%)         500M (14%)     2d18h
  kube-system                 etcd-k8s-1                       100m (2%)     0 (0%)      100Mi (3%)       0 (0%)         2d18h
  kube-system                 kube-apiserver-k8s-1             250m (6%)     0 (0%)      0 (0%)           0 (0%)         2d18h
  kube-system                 kube-controller-manager-k8s-1    200m (5%)     0 (0%)      0 (0%)           0 (0%)         2d18h
  kube-system                 kube-proxy-s8zsw                 0 (0%)        0 (0%)      0 (0%)           0 (0%)         2d18h
  kube-system                 kube-scheduler-k8s-1             100m (2%)     0 (0%)      0 (0%)           0 (0%)         2d18h
  kube-system                 nodelocaldns-njzgg               100m (2%)     0 (0%)      70Mi (2%)        170Mi (5%)     2d18h
Allocated resources:
  (Total limits may be over 100 percent, i.e., overcommitted.)
  Resource           Requests        Limits
  --------           --------        ------
  cpu                900m (23%)      300m (7%)
  memory             242257920 (6%)  678257920 (19%)
  ephemeral-storage  0 (0%)          0 (0%)
  hugepages-2Mi      0 (0%)          0 (0%)
Events:
  Type    Reason                   Age                From        Message
  ----    ------                   ----               ----        -------
  Normal  Starting                 39m                kubelet     Starting kubelet.
  Normal  NodeAllocatableEnforced  39m                kubelet     Updated Node Allocatable limit across pods
  Normal  NodeHasSufficientMemory  39m (x8 over 39m)  kubelet     Node k8s-1 status is now: NodeHasSufficientMemory
  Normal  NodeHasNoDiskPressure    39m (x8 over 39m)  kubelet     Node k8s-1 status is now: NodeHasNoDiskPressure
  Normal  NodeHasSufficientPID     39m (x7 over 39m)  kubelet     Node k8s-1 status is now: NodeHasSufficientPID
  Normal  Starting                 37m                kube-proxy  Starting kube-proxy.
  ```
----
+ ### К оглавлению! [___Оглавление___](../README.md)